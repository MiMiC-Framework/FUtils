#include <FUtils/Ret/Either.h>

const char* EitherBadAccess::what() const {
    return "Attempt to access to a 'Either' type in a invalid state";
}
