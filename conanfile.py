from conans import ConanFile, CMake, tools
import os

class FUtilsConan(ConanFile):
    name = "FUtils"
    version = "0.1"
    license = "Raising The Floor"
    url = "https://gitlab.com/MiMiC-Framework/FUtils"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "tests": [True, False]}
    generators = "cmake"
    requires = (
        "gtest/1.8.0@bincrafters/stable",
        "CVariant/0.1@javjarfer/testing",
        "UErrno/0.1@javjarfer/testing",
        "StdWrapper/0.1@javjarfer/testing"
    )
    default_options = (
        "shared=False",
        "tests=False"
    )

    def source(self):
        self.run("git clone git@gitlab.com:MiMiC-Framework/FUtils.git")
        self.run("cd FUtils && git checkout develop")

    def build(self):
        cmake = CMake(self)
        shared = "-DBUILD_SHARED_LIBS=ON" if self.options.shared else ""
        tests = "-DCMAKE_BUILD_TESTS=ON" if self.options.tests else ""
        self.run('cmake FUtils %s %s %s' % (cmake.command_line, shared, tests))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="FUtils/include")
        self.copy("*FUtils.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["FUtils"]
