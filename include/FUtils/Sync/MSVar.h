#ifndef MS_E_SVAR_H
#define MS_E_SVAR_H

#include <FUtils/Ret/Either.h>

#include <mutex>
#include <condition_variable>
#include <utility>
#include <functional>
#include <type_traits>
#include <atomic>
#include <array>

template <typename Payload, std::size_t Sz>
struct MSVar {
private:
    std::mutex m;
    std::condition_variable cv;
    std::array<bool,Sz> sms;
    Payload p;
    MSVar(const MSVar& s) = delete;
    MSVar& operator=(MSVar other) = delete;
public:
    MSVar(Payload&& p) : p(std::move(p)) {
        for (auto& k : sms) {
            k = false;
        }
    }

    /**
     * @brief
     * Returns the value of a SVar by value.
     */
    template <int smKey, typename A, std::size_t S>
    friend A takeMSVar(MSVar<A,S>& s);
    /**
     * @brief
     * Takes the value of a shared var, pass it to a function by reference
     * and returns and RValue corresponding to the result of applying that function.
     */
    template <typename A, std::size_t S, typename R>
    friend R takMSVar(MSVar<A,S>& s, std::function<R(A&)> f, int smKey);
    /**
     * @brief
     * Puts a new value into a SVar.
     */
    template <int smKey, typename A, std::size_t S>
    friend void putMSVar(MSVar<A,S>& s, A&& a);
};

template <int smKey, typename A, std::size_t S>
A takeMSVar(MSVar<A,S>& s) {
    static_assert(smKey < S || smKey > 0, "Not valid semaphore key");

    std::unique_lock<std::mutex> lock(s.m);
    s.cv.wait(lock, [&s]() { return s.sms[smKey]; });
    s.sms[smKey] = false;

    return std::move(s.p);
}

template <int smKey, typename A, std::size_t S>
void putMSVar(MSVar<A,S>& s, A&& a) {
    static_assert(smKey < S || smKey > 0, "Not valid semaphore key");

    std::unique_lock<std::mutex> lock(s.m);

    s.p = std::move(a);
    s.sms[smKey] = true;

    lock.unlock();
    s.cv.notify_all();
}

#endif
