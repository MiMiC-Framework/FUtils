#ifndef MS_LOCK
#define MS_LOCK

/**
 * @brief
 *  This datatype has the mechanisms to safely accesing a WMutex.
 * @details
 *  RAII implementation of a scoped block for the mutex. The lock have the same
 *  lifetime as this object, when the destructor of this Lock is called,
 *  WMutex ownership gets released.
 *
 *  **Example:**
 *  @code{.cpp}
 *  WMutex<int> m {0};
 *  {
 *      Lock<int> _lock(m);
 *      (*_lock) = 2;
 *  }
 *  @endcode
 *
 *  During Lock lifetime a user can ask for a safe reference to Mutex payload, using
 *  one of the providers operators, and access the payload in a thread safe way.
 */
template <typename A>
struct Lock {
private:
    /**
     * @brief
     *  Reference to the payload stored inside the Mutex this Lock is going to
     *  guard.
     */
    A& _p;
    /**
     * @brief
     *  Mutex ownership wrapper for protecting internal payload.
     * @details
     *  Initialized at instanciation time, and destroyed when destructor gets
     *  called.
     */
    unique_lock<Mutex> _lock;
public:
    /**
     * @brief
     *  Deleted copy constructor.
     * @details
     *  Lock is supposed to have an automatic destruction when going out of
     *  scope.
     */
    Lock(const Lock<A>& other) = delete;
    /**
     * @brief
     *  Deleted copy assignment opreator.
     * @details
     *  Lock is supposed to have an automatic destruction when going out of
     *  scope.
     */
    Lock& operator=(Lock other) = delete;
    /**
     * @brief
     *  Deleted move constructor.
     * @details
     *  Lock is supposed to have an automatic destruction when going out of
     *  scope.
     */
    Lock(Lock<A>&& other) = delete;
    /**
     * @brief
     *  Constructor that handles an external payload and a primitive Mutex for
     *  guarding that payload.
     * @param data
     *  Reference to external payload to guard.
     * @param m
     *  Primitive external Mutex to lock the payload access.
     */
    Lock(A& data, Mutex& m) : _p(data) {
        this->_lock = unique_lock<Mutex>(m);
    }
    /**
     * @brief
     *  Constructor receving a WMutex by reference.
     * @param m
     *  Mutex which lock is going to be requested.
     */
    Lock(WMutex<A>& m) : _p(m._p) {
        this->_lock = unique_lock<Mutex>(m._m);
    }
    /**
     * @brief
     *  Manually unlocks the internal guard.
     */
    void unlock() {
        this->_lock.unlock();
    }
    /**
     * @brief
     *  Retrieve the internal payload by reference.
     * @return
     *  A reference to the internal payload.
     */
    A& extract() {
        return _p;
    }
    /**
     * @brief
     *  Retrieve the internal payload by reference.
     * @return
     *  A reference to the internal payload.
     */
    A& operator*() {
        return _p;
    }
    /**
     * @brief
     *  Suspend thread execution using the internal lock and waits until the
     *  supplied condition variable receive the notified signal.
     * @param cv
     *  Condition variable for blocking the thread.
     * @warning
     *  This function can suffer from sporious wakeups, taking care of handling
     *  the wakeups with a proper waiting for condition loop is caller's work.
     */
    void wait(CV& cv) {
        cv.wait(this->_lock);
    }
    /**
     * @brief
     *  Suspend thread execution using the internal lock and waits until the
     *  supplied condition variable receive the notified signal.
     *
     * **Note**: Same function as the member one but in prefix notation.
     * @param cv
     *  Condition variable for blocking the thread.
     * @warning
     *  This function can suffer from sporious wakeups, taking care of handling
     *  the wakeups with a proper waiting for condition loop is caller's work.
     */
    template <typename A>
    friend void wait(CV& cv, Lock<A>& l);
    /**
     * @brief
     *  Suspend thread execution using the internal lock and waits until the
     *  supplied condition variable receive the notified signal and the supplied
     *  function that check the wakeup condition returns **true**.
     *
     * **Note**: Same function as the member one but in prefix notation.
     * @param cv
     *  Condition variable for blocking the thread.
     * @param check
     *  Function that verifies the condition that needs to take place in order
     *  to wake up the thread.
     */
    template < typename A,
               typename Fn,
               typename = EnIf< IsSame<bool,IRes<Fn>> > >
    friend void wait(CV& cv, Lock<A>& l, const Fn& check);
};

template <typename A>
void wait(CV& cv, Lock<A>& l) {
    cv.wait(l._lock);
}

template < typename A,
           typename Fn,
           typename = EnIf< IsSame<bool,IRes<Fn>> > >
void wait(CV& cv, Lock<A>& l, const Fn& check) {
    cv.wait(
        l._lock,
        [&check]() {
            return check();
        }
    );
}

#endif
