/**
 * @file   SVar.h
 * @author Javier Jaramago Fernández(jj@raisingthefloor.org)
 * @date   October, 2017
 * @brief  SVar datatype implementation.
 * @details
 *  Syncronization primitive that provides a secure communication channel for sharing data
 *  between threads in a simple push/take style.
 */

#ifndef MS_SVAR_H
#define MS_SVAR_H

#include <FUtils/Ret/Either.h>
#include <FUtils/Sync/WMutex.h>
#include <FUtils/Sync/Lock.h>

#include <Std/CV.h>
#include <Std/Functional.h>
#include <Std/TypeTraits.h>
#include <Std/Utility.h>

/**
 * @brief
 *  Synchronization Variable.
 * @details
 *  A syncronization primitive that provides a secure communication channel for sharing data
 *  between threads in a simple push/take style.
 *
 *  The SVar has two possible states: **empty**  and **filled**. When created the SVar is
 *  empty, so any attempt for taking the value from it, will result in a **wait** until
 *  a **put method** has been called to put a value into it. The SVar is created with
 *  an initial value inside, given by its unique constructor, this behavior have two
 *  main reasons:
 *      -# Delegating the responsability of leaving the SVar in a internal known state
 *         to the caller.
 *      -# Allow for updating the SVar because it was left previously in a known
 *         internal state.
 *
 *  The **updates methods** the only ones that doesn't follow the previous
 *  described behavior, this methods perform an atomic access to the SVar, update
 *  it's internal value and left it in a **filled state**.
 *
 *  @note
 *   A more clever impl is possible. But for now this is enought.
 */
template <typename A>
struct SVar : WMutexAccessor<A> {
private:
    /**
     * @brief
     *  Syncronization primitive.
     */
    WMutex<A> _m;
    /**
     * @brief
     *  Conditional variable for asking for internal mutex ownership.
     */
    CV _cv;
    /**
     * @brief
     *  Boolean acting like a flag showing the internal state of the SVar.
     * @details
     *  This boolean it's true when the variable has been filled with a
     *  new value using a 'put' operation, and become false after any
     *  'take' operation take place.
     */
    bool _filled = false;
    /**
     * @brief
     *  Implicitely deleted copy constructor.
     * @details
     *  This object could not be copy because it holds non-copyable primitives.
     *  It's only supposed to be shared as a reference.
     */
    SVar(const SVar& s) = delete;
    /**
     * @brief
     *  Implicetly deleted function assigment operator.
     * @details
     *  This object could not be copy because it holds non-copyable
     *  primitives. It's only supposed to be shared as a reference.
     */
    SVar& operator=(SVar other) = delete;
public:
    /**
     * @brief
     *  Constructor.
     * @param p
     *  Payload to be stored inside the SVar, variable is moved if possible.
     */
    SVar(A&& p) : _m(std::move(p)) {}
    /**
     * @brief
     *  Returns the value holded inside SVar by value, this function **will wait**
     *  until the SVar has been filled with a value using a **put method**.
     * @param s
     *  The SVar which value is going to be accessed.
     * @returns
     *  The value inside the SVar in a 'by-value' style.
     */
    template <typename A>
    friend A takeSVar(SVar<A>& s);
    /**
     * @brief
     *  Returns the value holded inside SVar, after applying a function to
     *  it, which receives this value **by reference**. How this value is returned
     *  depends of the implementation of the function passed as parameter.
     *  This function **will wait** until the SVar has been filled with a value
     *  using a **put method**.
     * @param s
     *  The SVar which value is going to be accessed.
     * @param f
     *  The function to be applied to the internal value of the SVar. This function
     *  should receive its parameter by reference.
     */
    template <typename A, typename Fn, typename = EnIf<IsInv<Fn,A&>> >
    friend auto takeSVar(SVar<A>& s, const Fn& f) -> IRes<Fn, A&>;
    /**
     * @brief
     *  Tries to return the SVar's payload, by moving it. Doing this normally
     *  would leave the SVar in a 'indetermined state', for avoing such condition,
     *  the function provided **fn** is applied, taking the internal value **by refernce**
     *  leaving the payload in a well known state by the caller.
     *
     * **Note:** This is behavior is particulary insteresting when several clients are
     * updating the SVar's payload, and you want to retrieve the new actualized results
     * while leaving the SVar in a well known state for future updates.
     * @param s
     *  The SVar which value is going to be accessed.
     * @param fn
     *  Function taking the SVar's payload **by reference**, for initializing the it,
     *  after retrieving its content.
     */
    template <typename A, typename Fn, typename = EnIf<IsInv<Fn,A&>> >
    friend A takeAndInitSVar(SVar<A>& s, const Fn& fn);
    /**
     * @brief
     *  Returns the internal value hold by the SVar **by copy**, without modifying it. This
     *  operation **will wait** until the SVar is in a **filled state**. This
     *  operation have **NO IMPACT** on the internal state of the SVar, it will continue
     *  preserving it's internal previous state of **filled** or **empty**.
     * @param s
     *  The SVar which internal value is going to be retrieved.
     * @return
     *  The internal value of the SVar received by parameter by copy.
     */
    template <typename A>
    friend A readSVar(SVar<A>& s);
    /**
     * @brief
     *  Puts a new value into a SVar.
     * @details
     *  The payload parameter of this function is **not supposed to be used** after being supplied
     *  to this function. It should be moved if possible, leaving this function handling it's
     *  lifetime.
     * @param s
     *  The SVar which is going to receive a new payload.
     * @param a
     *  The payload that is going to be moved inside the SVar.
     */
    template <typename A>
    friend void putSVar(SVar<A>& s, A&& a);
    /**
     * @brief
     *  Update the SVar applying a function that takes its payload by reference.
     * @details
     *  This function doesn't wait to any internal state of the SVar, it simply updates
     *  the SVar's payload atomically, using the function supplied as a parameter, which
     *  recieves this payload **by reference**.
     *
     *  **Warning:** This function **assumes** SVar's payload is in **usable** state.
     * @param s
     *  The SVar that is going to be updated.
     * @param fn
     *  The function that is going to be applied to SVar's payload.
     */
    template <typename A, typename Fn, typename = EnIf<IsInv<Fn,A&>> >
    friend void updSVar(SVar<A>& s, const Fn& fn);
    /**
     * @brief
     *  Update the supplied SVar applying the provided function that takes SVar's
     *  payload and the third parameter by reference.
     * @details
     *  This is just a convenience function for being able to pass something by
     *  reference to the function that is going to update SVar's payload.
     * @param s
     *  The SVar that is going to be updated.
     * @param fn
     *  The function that is going to be applied to SVar's payload, it should receive
     *  two parameters, both by reference.
     * @param b
     *  The second parameter that is going to be passed to the function 'fn'.
     */
    template <typename A, typename B, typename Fn, typename = EnIf<IsInv<Fn,A&,B&&>> >
    friend void updSVar(SVar<A>& s, const Fn& fn, B&& b);
    /**
     * @brief
     * Takes the value of a SVar, pass it to a function by reference, which
     * returns and Either, with the new value for the SVar if the previous
     * computation succeeded, or and Error if not. If the new value is not an error
     * the SVar is updated with it. An error or result expressing the result of the global
     * computation is returned.
     */
     template <typename A, typename R, typename E>
     friend Either<R,E> updSVar(SVar<A>& s, const Fn<Either<R,E>(A&)>& fn);

    // ========================================================================
    //                  FAST COMPUTATIONS FAMILY FUNCTIONS:
    // ========================================================================

    /**
     * @brief
     *  Takes the value of a SVar, pass it to a function by reference, which
     *  returns and Either, with the new value for the SVar if the previous
     *  computation succeeded, or and Error if not. If the new value is not an error
     *  the SVar is updated with it. An error expressing the result of the global
     *  computation is returned.
     * @param s
     *  The Svar from which the value is going to be taked.
     * @param t
     *  The Svar that is going to receive the value if the computation from the 'f'
     *  function succeed.
     * @param success
     *  The error code for a succesfull operation.
     * @param f
     *  The function to be executed.
     * @result
     *  Returns the resulting error code of the operation.
     */
    template <typename A, typename R, typename E>
    friend E takePutSVar(SVar<A>& s, SVar<A>& t, E&& success, Fn<Either<R,E>(A&)> fn);

    // ========================================================================
};

template <typename A>
A takeSVar(SVar<A>& s) {
    Lock<A> lock(s._m);
    wait(s._cv, lock, [&s]() { return s._filled; });

    s._filled = false;

    return std::move(s.takeWMutex(s._m));
}

template <typename A, typename Fn, typename = EnIf<IsInv<Fn,A&>> >
auto takeSVar(SVar<A>& s, const Fn& f) -> IRes<Fn, A&> {
    Lock<A> lock(s._m);
    wait(s._cv, lock, [&s]() { return s._filled; });

    auto res = f(*lock);
    s._filled = false;

    lock.unlock();

    return res;
}

template <typename A, typename Fn, typename = EnIf<IsInv<Fn,A&>> >
A takeAndInitSVar(SVar<A>& s, const Fn& fn) {
    Lock<A> lock(s._m);
    wait(s._cv, lock, [&s]() { return s._filled; });

    A res = std::move(s.takeWMutex(s._m));

    // Leave the SVar in a initialized and known state.
    fn(*lock);
    s._filled = true;

    lock.unlock();

    return std::move(res);
}

template <typename A>
A readSVar(SVar<A>& s) {
    Lock<A> lock(s._m);
    wait(s._cv, lock, [&s]() { return s._filled; });
    s._filled = false;

    return s.takeWMutex(s._m);
}

template <typename A>
void putSVar(SVar<A>& s, A&& a) {
    Lock<A> lock(s._m);
    wait(s._cv, lock, [&s]() { return !s._filled; });

    *lock = std::move(a);
    s._filled = true;

    lock.unlock();
    s._cv.notify_all();
}

template <typename A, typename Fn, typename = EnIf<IsInv<Fn,A&>> >
void updSVar(SVar<A>& s, const Fn& fn) {
    Lock<A> lock(s._m);

    fn(*lock);
    s._filled = true;

    lock.unlock();
    s._cv.notify_all();
}

template <typename A, typename B, typename Fn, typename = EnIf<IsInv<Fn,A&,B&&>> >
void updSVar(SVar<A>& s, const Fn& fn, B&& b) {
    Lock<A> lock(s._m);

    fn(*lock, std::forward<B>(b));
    s._filled = true;

    lock.unlock();
    s._cv.notify_all();
}

template <typename A, typename R, typename E>
Either<R,E> updSVar(SVar<A>& s, const Fn<Either<R,E>(A&)>& fn) {
    Lock<A> lock(s._m);

    auto res = fn(*lock);
    s._filled = true;

    lock.unlock();

    return res;
}

template <typename A, typename R, typename E>
E takePutSVar(SVar<A>& s, SVar<A>& t, E&& success, Fn<Either<R, E>(A&)> fn) {
    Lock<A> lockFrom(s._m);
    wait(s._cv, lockFrom, [&s]() { return s._filled; });

    E resVal = success;

    auto pRes = fn(*lockFrom);
    s._filled = false;

    lockFrom.unlock();

    PMatch(pRes,
        [&t](const R& r) {
            Lock<A> lockTo(t._m);

            *lockTo = r;
            t._filled = true;

            lockTo.unlock();
            t._cv.notify_all();
        },
        [&resVal](const E& e) {
            resVal = e;
        }
    );

    return resVal;
}

#endif
