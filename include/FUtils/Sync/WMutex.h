/**
 * @file   WMutex.h
 * @author Javier Jaramago Fernández(jj@raisingthefloor.org)
 * @date   October, 2017
 * @brief  SVar datatype implementation.
 * @details
 *  Syncronization primitives that provided thread safe access to a internal payload,
 *  stored inside a WMutex.
 */

#ifndef MS_WMUTEX_H
#define MS_WMUTEX_H

#include <Std/CV.h>
#include <Std/Functional.h>
#include <Std/Mutex.h>
#include <Std/TypeTraits.h>
#include <Std/Utility.h>

using std::unique_lock;

/// @cond
template <typename A>
struct Lock;
/// @endcond

/// @cond
template <typename A>
struct WMutexAccessor;
/// @endcond

/**
 * @brief
 *  Container for accessing a stored payload in a thread safe way.
 * @details
 *  This type represent the container for the stored payload, and doesn't provide any
 *  direct access to it. The only type through which you can access is the Lock,
 *  which provides the barrier and conditions to make the access to the payload **amotic**.
 */
template <typename A>
struct WMutex {
private:
    /**
     * @brief
     *  Sincronization primitive.
     */
    Mutex   _m;
    /**
     * @brief
     *  Payload stored in the type.
     */
    A       _p;

public:
    /**
     * @brief
     *  Deleted copy construtor.
     * @details
     *  WMutex aren't supposed to be copied, they are not supposed to be a way of
     *  dupplicating their payloads. They are supposed to be shared by reference
     *  to the multiple clients that want to modify its payload.
     */
    WMutex(const WMutex<A>& other) = delete;
    /**
     * @brief
     *  Deleted move construtor.
     * @details
     *  There is no point in moving a WMutex. They are supposed to be shared by reference
     *  to the multiple clients that want to modify its payload.
     */
    WMutex(WMutex<A>&& other) = delete;
    /**
     * @brief
     *  Constructor taking the payload that the mutex is going to store inside as
     *  an argument.
     * @details
     *  WMutex are supposed to handle the lifetime of their payloads, for correctness,
     *  they should not be modified one they have been passed to them. For ensuring
     *  this behavior would be better if their payloads are **moved** inside them.
     * @param a
     *  The payload that is going to be moved inside the WMutex.
     */
    WMutex(A&& a) : _p(std::move(a)) {}
    /**
     * @brief
     *  Type that provides the mechanisms for safely accessing the WMutex payload.
     */
    friend struct Lock<A>;
    friend struct WMutexAccessor<A>;
};

/**
 * @brief
 *  Type to create anohter sync-types that can move the resources out of a WMutex.
 */
template <typename A>
struct WMutexAccessor {
protected:
    /**
     * @brief takeWMutex
     * @param m
     *  The WMutex which payload is going to be moved out.
     * @return
     *  The moved payload of the WMutex passed as a parameter.
     * @return
     */
    A takeWMutex(WMutex<A>& m) {
        return std::move(m._p);
    }
};

#endif
