#ifndef MS_SRVAR_H
#define MS_SRVAR_H

#include <FUtils/Ret/Either.h>
#include <FUtils/Sync/WMutex.h>
#include <FUtils/Sync/Lock.h>

#include <Std/CV.h>
#include <Std/Functional.h>
#include <Std/TypeTraits.h>
#include <Std/Utility.h>

/**
 * @brief
 *  Struct holding the minimum syncronization primitives for creating atomic access to a
 *  payload and being able to create certain waiting conditions over a semaphore.
 * @note
 *  This are the minimum primitives to create a communication channel.
 * @details
 *  Struct containing a group of refernces to the necessaries sync primitives to create
 *  a communication channel.
 * @var m
 *  WMutex for creating the atomic access to the protected payload.
 * @var cv
 *  Conditional variable for suspending thread execution under some flag conditions.
 * @var flag
 *  Boolean flag that should used for creating barrier conditions.
 */
template <typename A>
struct SyncRes {
    WMutex<A>&  m;
    CV&         cv;
    bool&       flag;
};

/**
 * @brief
 *  The SRVar (Shared Resources Var) is a SVar which synchronization resources
 *  are shared references.
 * @details
 *  The whole point of constructing a SVar in which sync resources are handled in a
 *  bare way in the other endpoint, is to provide the extra flexibility that a simple
 *  communication channel lacks about.
 *
 *  Put/Take style from a channel alike sync interface doesn't give you access to the
 *  flags that provide the **source of information** to decide how to react on **notifying**
 *  events. That makes imposible to have **different wakeup logics** depending of the
 *  source of the event unless having access to that flags.
 */
template <typename A>
struct SRVar : WMutexAccessor<A> {
private:
    WMutex<A>&  _m;
    CV&         _cv;
    bool&       _filled;

public:
    SRVar(const SRVar& s) = delete;
    SRVar& operator=(SRVar other) = delete;
    SRVar(SRVar&& s) = delete;

    /**
     * @brief
     *  Cosntructor that initialices the SRVar from references.
     * @param sps
     *  Reference to the necessaries synchronization primitives.
     */
    SRVar(SyncRes<A>& sps) : _m(sps.m), _cv(sps.cv), _filled(sps.flag) {}
    /**
     * @brief
     * Returns the value of a SVar by value.
     */
    template <typename A>
    friend A takeSRVar(SRVar<A>& s);
    /**
     * @brief
     * Returns the value of a SVar leaving it empty, and then applies the function 'fn'
     * to leave it in a valid state.
     */
    template <typename A, typename Fn>
    friend A takeAndInitSRVar(SRVar<A>& s, Fn fn);
    /**
     * @brief Returns the internal value hold by the SVar by copy, without modifying it.
     * @param s The SVar which internal value is going to be retrieved.
     * @return The internal value of the SVar received by parameter by copy.
     */
    template <typename A>
    friend A readSRVar(SRVar<A>& s);
    /**
     * @brief
     * Puts a new value into a SVar.
     */
    template <typename A>
    friend void putSRVar(SRVar<A>& s, A&& a);
    /**
     * @brief TODO
     */
    template <typename A, typename Fn>
    friend void updSRVar(SRVar<A>& s, Fn& f);
    /**
     * @brief TODO
     */
    template <typename A, typename Fn, typename B>
    friend void updSRVar(SRVar<A>& s, Fn& f, B&& b);
    /**
     * @brief TODO
     */
    template <typename A>
    friend void waitFilled(Lock<RefWrapper<A>>& l, SRVar<A>& s);
    /**
     * @brief TODO
     */
    template <typename A>
    friend void waitEmpty(Lock<RefWrapper<A>>& l, SRVar<A>& s);
};

template <typename A>
A takeSRVar(SRVar<A>& s) {
    Lock<A> lock(s._m);
    wait(s._cv, lock, [&s]() { return s.filled; });

    s._filled = false;

    return std::move(s.takeWMutex(s.m));
}

template <typename A, typename Fn>
A takeAndInitSRVar(SRVar<A>& s, Fn fn) {
    Lock<A> lock(s._m);
    wait(s._cv, lock, [&s]() { return s.filled; });

    A res = std::move(s.takeWMutex(s._m));

    // Leave the SVar in a initialized and known state.
    fn(*lock);
    s._filled = true;

    lock.unlock();

    return std::move(res);
}

template <typename A>
A readSRVar(SRVar<A>& s) {
    Lock<A> lock(s._m);
    wait(s._cv, lock, [&s]() { return s._filled; });
    s._filled = false;

    return s.p;
}

template <typename A>
void putSRVar(SRVar<A>& s, A&& a) {
    Lock<A> lock(s._m);
    wait(s._cv, lock, [&s]() { return !s._filled; });

    *lock = std::move(a);
    s._filled = true;

    lock.unlock();
    s._cv.notify_all();
}

template <typename A, typename Fn>
void updSRVar(SRVar<A>& s, Fn& f) {
    Lock<A> lock(s._m);

    f(*lock);
    s._filled = true;

    lock.unlock();
    s._cv.notify_all();
}

template <typename A, typename Fn, typename B>
void updSRVar(SRVar<A>& s, Fn& f, B&& b) {
    Lock<A> lock(s._m);

    f(*lock, std::forward<B>(b));
    s._filled = true;

    lock.unlock();
    s._cv.notify_all();
}

template <typename A>
void waitFilled(Lock<RefWrapper<A>>& l, SRVar<A>& s) {
    s._cv.wait(
        l._lock,
        [&s]() {
            return s._filled;
        }
    );
}

template <typename A>
void waitEmpty(Lock<RefWrapper<A>>& l, SRVar<A>& s) {
    s._cv.wait(
        l._lock,
        [&s]() {
            return !s._filled;
        }
    );
}

template <typename A, typename R, typename E>
E takePutSRVar(SRVar<A>& s, SRVar<A>& t, E&& success, Fn<Either<R, E>(A&)> fn) {
    Lock<A> lockFrom(s._m);
    wait(s._cv, lockFrom, [&s]() { return s._filled; });

    E resVal = success;

    auto pRes = fn(*lockFrom);
    s._filled = false;

    lockFrom.unlock();

    PMatch(pRes,
        [&t](const R& r) {
            Lock<A> lockTo(t._m);

            *lockTo = r;
            t._filled = true;

            lockTo.unlock();
            t._cv.notify_all();
        },
        [&resVal](const E& e) {
            resVal = e;
        }
    );

    return resVal;
}


#endif
