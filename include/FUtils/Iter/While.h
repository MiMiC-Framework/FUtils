#ifndef MS_UTILS_H
#define MS_UTILS_H

#include <FUtils/Ret/Either.h>

#include <utility>
#include <functional>
#include <mutex>
#include <condition_variable>

template <typename S, typename E, typename... A>
Either<S,E> While(S&& ini, std::function<bool(Either<S,E>)> final, std::function<Either<S,E>(S&&, A&&...)> compute, A&&... a) {
    auto state = Either<S,E>(std::forward<S>(ini));

    while (!final(state)) {
        state = compute(std::move(state), std::forward<A>(a)...);
    }

    return state;
}

template <typename S, typename E, typename... A>
Either<S,E> WhileA(S&& ini, std::function<bool(Either<S,E>)> final, std::function<Either<S,E>(A&&...)> compute, A&&... a) {
    auto state = Either<S,E>(std::forward<S>(ini));

    while (!final(state)) {
        state = compute(std::forward<A>(a)...);
    }

    return state;
}

template <typename S, typename E, typename... A>
Either<S,E> WhileA(S&& ini, std::function<bool(Either<S,E>)> final, std::function<Either<S,E>(A&...)> compute, A&... a) {
    auto state = Either<S,E>(std::forward<S>(ini));

    while (!final(state)) {
        state = compute(std::forward<A>(a)...);
    }

    return state;
}

#endif
