#ifndef Either_H
#define Either_H

#include <utility>
#include <iostream>
#include <type_traits>

template <typename T>
using RmRef = typename std::remove_reference<T>::type;

class EitherBadAccess : std::exception {
public:
    const char *what() const;
};

template <typename A>
struct Left {
    A val;
    Left(A a) : val(a) {};
};

template <typename A>
Left<RmRef<A>> MLeft(A&& a) {
    return std::move(a);
}

template <typename A>
Left<A> MLeft(const A& a) {
    return {a};
}

template <typename B>
struct Right {
    B val;
    Right(B b) : val(b) {};
};

template <typename B>
Right<RmRef<B>> MRight(B&& b) {
    return std::move(b);
}

// This specialization is necessary in order to avoid const deduction for type
template <typename B>
Right<B> MRight(const B& b) {
    return {b};
}

/**
 * @class Either
 * @brief Class for repEitherenting left or failure in a single return type.
 * @details
 * ADT like class that can be accessed safely for matching its Eitherult. Should be
 * the prefered way of returning from functions that could return errors in cpp code.
 */
template <typename A, typename B>
class Either {
public:
    /**
     * @brief
     * Destructor for cleaning the Either type, it's only executed while
     * holding a val (complex type), non a error code (lives in the stack).
     */
    ~Either() {
        if (this->_isLeft) {
            this->lVal.~A();
        } else {
            this->rVal.~B();
        }
    }

    /**
     * @brief Move constructor for values.
     * @param val Value to be moved inside the type.
     */
    constexpr Either(Left<A>&& left) :
        lVal(std::move(left.val)), _isLeft(true) {}
    /**
     * @brief Copy constructor for values.
     * @param val Value to be copied inside the type.
     */
    constexpr Either(const Left<A>& left) : lVal(std::move(left.val)), _isLeft(true) {}

    /**
     * @brief Move constructor for error types.
     */
    constexpr Either(Right<B>&& right) : rVal(std::move(right.val)), _isLeft(false) {}
    /**
     * @brief Copy constructor for errors.
     */
    constexpr Either(const Right<B>& right) : rVal(std::move(right.val)), _isLeft(false) {}

    /**
     * @brief Copy constructor.
     * @param other Either type that is going to be copied inside this one.
     */
    constexpr Either(const Either& other) : _isLeft(other._isLeft) {
        if (other.isLeft()) {
            new (&this->lVal) A(other.lVal);
        } else {
            new (&this->rVal) B(other.rVal);
        }
    }

    /**
     * @brief Move constructor.
     * @param other Either type that is going to be moved inside this one.
     */
    constexpr Either(Either&& other) : _isLeft(other._isLeft) {
        if (other.isLeft()) {
            new (&this->lVal) A(std::move(other.lVal));
        } else {
            new (&this->rVal) B(std::move(other.rVal));
        }
    }

    /**
     * @brief Move assigment operator.
     * @param other Either type that is going to be moved inside this one.
     * @return A Either type filled with the content of the moved one.
     */
    constexpr Either& operator=(Either&& other) {
        if (_isLeft) {
            this->lVal.~A();
        } else {
            this->rVal.~B();
        }

        if (other.isLeft()) {
            new (&this->lVal) A(std::move(other.lVal));
            this->_isLeft = true;
        } else {
            new (&this->rVal) B(std::move(other.rVal));
            this->_isLeft = false;
        }

        return *this;
    }

    /**
     * @brief Copy assigment operator.
     * @param other Either type that is going to be copied inside this one.
     * @return A Either type filled with the content of the copied one.
     */
    constexpr Either& operator=(const Either& other) {
        if (&other == this) {
            return *this;
        }

        if (this->_isLeft) {
            this->lVal.~A();
        } else {
            this->rVal.~B();
        }

        if (other.isLeft()) {
            new (&this->lVal) A(other.lVal);
            this->_isLeft = true;
        } else {
            new (&this->rVal) B(other.rVal);
            this->_isLeft = false;
        }

        return *this;
    }

    /**
     * @brief Copy assigment operator for values.
     * @param val Value which is going to be used to fill the receiver type.
     * @return A Either type filled with the received value.
     */
    constexpr Either& operator=(const Left<A>& left) {
        if (_isLeft) {
            this->lVal.~A();
        } else {
            this->rVal.~B();
        }

        _isLeft = true;
        new (&this->lVal) A(left.val);

        return *this;
    }

    /**
     * @brief Copy assigment operator for errors.
     * @param _err Error which is going to be used to fill the receiver type.
     * @return A Either type filled with the received error.
     */
    constexpr Either& operator=(const Right<B>& right) {
        if (_isLeft) {
            this->lVal.~A();
        } else {
            this->rVal.~B();
        }

        _isLeft = false;
        this->rVal = right.val;

        return *this;
    }

    /**
     * @brief Function for checking if the Eitherult has a valid value.
     * @return True if the type contains a value, False if it contains an error.
     */
    constexpr const bool isLeft() const { return _isLeft; }

    template <typename FnL>
    constexpr auto leftMap(FnL const & fnL) const & -> Either<decltype(fnL(this->lVal)), B> {
        using NextEither = Either<decltype(fnL(lVal)), B>;

        if (_isLeft) {
            return NextEither(MLeft(fnL(lVal)));
        } else {
            return NextEither(MRight(rVal));
        }
    }

    template <typename FnL>
    constexpr auto leftMap(FnL const & fnL) && -> Either<decltype(fnL(this->lVal)), B> {
        using NextEither = Either<decltype(fnL(lVal)), B>;

        if (_isLeft) {
            return NextEither(MLeft(fnL(lVal)));
        } else {
            return NextEither(MRight(rVal));
        }
    }

    template <typename FnR>
    constexpr auto rightMap(FnR const & fnR) const & -> Either<A, decltype(fnR(this->rVal))> {
        using NextEither = Either<A, decltype(fnR(rVal))>;

        if (_isLeft) {
            return NextEither(MLeft(lVal));
        } else {
            return NextEither(MRight(fnR(rVal)));
        }
    }

    template <typename _A = A, typename _B = B>
    constexpr auto join() const & -> typename std::common_type<_A, _B>::type {
        if (this->_isLeft) {
             return this->lVal;
        }

        return this->rVal;
    }

    /**
     * @brief Friend function to safely access to the contents of the type.
     * @param Either Either type we want to match.
     * @param fv Function that receives the possible value inside the type.
     * @param fe Function that receives the possible error inside the type.
     * @returns The Eitherult of applying the correct function to the inner value of the type.
     */
    template <typename A, typename B, typename FnL, typename FnR>
    friend constexpr auto PMatch(const Either<A,B>& Either, FnL&& fnL, FnR&& fnR);

    /**
     * @brief Friend function for accessing in a unsafe way to the value inside the type.
     * @throw Either_bad_access expection if the type is in a invalid state.
     * @returns The value inside the type.
     */
    template <typename A, typename B>
    friend A& unsafeValAccess(const Either<A,B>& Either);
    /**
     * @brief Friend function for accessing in a unsafe way to the error inside the type.
     * @throw Either_bad_access expection if the type is in a invalid state.
     * @returns The error inside the type.
     */
    template <typename A, typename B>
    friend B& unsafeErrAccess(const Either<A,B>& Either);

private:
    bool _isLeft = false;

    /**
     * @brief Anonimous union repEitherenting the internal state of the Either.
     */
    union {
        A lVal;
        B rVal;
    };

    /**
     * @brief
     * Static assertion for making sure that the type is being created using
     * a whatever type for holding as a left, and a integral or enum type for
     * error.
     */
    /**
     * In a ideal situation we could request Error type to fill certain properties,
     * but dealing with third party libraries, anything could be an error type.
     *
     * Static_assert(
     *     std::is_enum<Right>::value || std::is_integral<Right>::value,
     *     "Right type should be enum or integral type"
     * );
     */
};

template <typename A, typename B>
A& unsafeLeftAccess(const Either<A,B>& Either) {
#ifndef UNSAFE_EITHER
    if (!Either._isLeft) {
        throw EitherBadAccess();
    }
#endif
    return &Either.lVal;
}

template <typename A, typename B>
B& unsafeRighAccess(const Either<A,B>& Either) {
#ifndef UNSAFE_EITHER
    if (Either._isLeft) {
        throw EitherBadAccess();
    }
#endif
    return &Either.rVal;
}

#ifdef _MSC_VER

namespace detail {
  template <typename T>
  using always_void = void;

  template <typename Expr, typename Enable = void>
  struct is_callable_impl
    : std::false_type
  {};

  template <typename F, typename ...Args>
  struct is_callable_impl<F(Args...), always_void<std::result_of_t<F(Args...)>>>
    : std::true_type
  {};
}

template <typename Expr>
struct is_callable
  : detail::is_callable_impl<Expr>
{};

#endif

template <typename A, typename B, typename FnL, typename FnR>
constexpr auto PMatch(const Either<A,B>& Either, FnL&& fnL, FnR&& fnR) {
#ifndef _MSC_VER
    // This isn't working for MSVC due to C++17 incomplete implementation.
    static_assert(
        std::is_invocable<FnL, A>::value,
        "Either: Matching error, FnL function should be able to take type A."
    );
    static_assert(
        std::is_invocable<FnR, B>::value,
        "Either: Matching error, FnR function should be able to take type B."
    );
#else
    // This isn't working for MSVC due to C++17 incomplete implementation.
    static_assert(
        is_callable<FnL(A)>::value,
        "Either: Matching error, FnL function should be able to take type A."
    );
    static_assert(
        is_callable<FnR(B)>::value,
        "Either: Matching error, FnR function should be able to take type B."
    );
#endif
    if (Either._isLeft) {
        return fnL(Either.lVal);
    } else {
        return fnR(Either.rVal);
    }
}

// NEXT ITERATION: UNPACK CALLING

#endif
