#include <FUtils/Sync/MSVar.h>

#include <utility>
#include <gtest/gtest.h>

template <typename T, std::size_t S>
using array = std::array<T,S>;

enum Semaphores {
    toWorker    = 0,
    toMain      = 1
};

TEST(MSVar, ChangeMSVarValue) {
    MSVar<int,2> sVar { 0 };

    std::thread get(
        [&sVar]() {
            auto newVal = takeMSVar<toWorker>(sVar);
            ASSERT_EQ(newVal, 5);
            putMSVar<toMain>(sVar, newVal + 1);
        }
    );

    std::thread put(
        [&sVar]() {
            putMSVar<toWorker>(sVar, 5);
            auto workerRes = takeMSVar<toMain>(sVar);
            ASSERT_EQ(workerRes, 6);
        }
    );

    put.join();
    get.join();

    ASSERT_TRUE(true);
}
