#include <FUtils/Sync/SRVar.h>

#include <Std/Queue.h>
#include <Std/Utility.h>
#include <Std/Vector.h>

#include <gtest/gtest.h>

bool checkFlags(const std::vector<bool*>& flags) {
    for(auto flag : flags) {
        if (*flag == true) { return true; }
    }
    return false;
}

TEST(SRVar, MultipleResourcesHandling) {
    WMutex<Queue<int>> qMutex { Queue<int>{} };
    CV qCV;
    bool qFlag = false;

    SyncRes<Queue<int>> qSyncRes { qMutex, qCV, qFlag };
    SRVar<Queue<int>> qSRVar  { qSyncRes };

    WMutex<Vector<int>> vMutex { Vector<int>{} };
    CV vCV;
    bool vFlag = false;

    SyncRes<Vector<int>> vSyncRes { vMutex, vCV, vFlag };
    SRVar<Vector<int>> vSVar { vSyncRes };

    Vector<bool*> flags { &vFlag, &qFlag };

    std::thread queueWorker([&qSRVar]() {
        putSRVar(qSRVar, Queue<int>{ 15 });
    });

    std::thread main(
        [&qSyncRes,&flags]() {
            Lock<Queue<int>> lock(qSyncRes.m);
            wait(qSyncRes.cv, lock, [&flags] { return checkFlags(flags); });

            if (*flags[1] == true) {
                // Extract and check the value
                // ============================================================
                auto val = (*lock).front();
                ASSERT_EQ(val, 15);
                // Clear the queue and set the flag
                // ============================================================
                (*lock).clear();
                *flags[1] = false;
            } else {
                ASSERT_FALSE(true);
            }
        }
    );

    queueWorker.join();
    main.join();
}
