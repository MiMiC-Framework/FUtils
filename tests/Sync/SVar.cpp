#include <FUtils/Sync/SVar.h>

#include <Std/Queue.h>
#include <Std/Utility.h>
#include <Std/Vector.h>

#include <uerrno/uerrno.h>

#include <gtest/gtest.h>
#include <chrono>
#include <thread>

TEST(SVar, SimpleTakePutCommunication) {
    SVar<int> sVar { 0 };

    std::thread get(
        [&sVar]() {
            auto newVal = takeSVar(sVar);
            ASSERT_EQ(newVal, 5);
        }
    );

    std::thread put(
        [&sVar]() {
            putSVar(sVar, 5);
        }
    );

    put.join();
    get.join();

    ASSERT_TRUE(true);
}

TEST(SVar, TakeSVarValueAndModify) {
    SVar<int> sVar { 0 };

    std::thread get(
        [&sVar]() {
            int newVal = takeSVar(sVar,
                [](int& val) -> int {
                     return val + 1;
                }
            );
            ASSERT_EQ(newVal, 6);
        }
    );

    std::thread put(
        [&sVar]() {
            putSVar(sVar, 5);
        }
    );

    put.join();
    get.join();
}

TEST(SVar, takeAndInitSVar) {
    SVar<int> s {0};
    putSVar(s, 0);
    takeAndInitSVar(s, [](int& val) -> void { val = 1; });
    ASSERT_EQ(takeSVar(s), 1);
}

TEST(SVar, UpdSVarVoid) {
    SVar<int> s {0};
    updSVar(s, [](auto& i) { i = 1;} );
    ASSERT_EQ(takeSVar(s), 1);
}

TEST(SVar, UpdSVarVoidOptParam) {
    SVar<int> s {0};
    updSVar(s, [](auto& i, auto&& b) { i = 1 + b; }, 1);
    ASSERT_EQ(takeSVar(s), 2);
}

TEST(SVar, UpdSVarEither) {
    SVar<int> s {0};

    Fn<Either<int,errno_t>(int&)> increment{
        [](int& v) -> Either<int, errno_t> {
            return Left<int>(v + 1);
        }
    };

    auto res = updSVar(s, increment);

    PMatch(res,
        [](auto value) {
            ASSERT_EQ(value, 1);
        },
        [](auto) {
            ASSERT_FALSE(true);
        }
    );
}

TEST(SVar, TakePutSVarAsChannelToReturn) {
    using namespace std::chrono_literals;

    SVar<int> toWorker {0};
    SVar<int> toMain {0};

    std::thread worker(
        [&toMain,&toWorker]() {
            auto newVal =
                takePutSVar(
                    toWorker,
                    toMain,
                    fnerr_t(E_FN_SUCCESS),
                    Fn<Either<int,fnerr_t>(int&)>(
                        [](auto& val) -> Either<int,fnerr_t> {
                            return Left<int>(val + 1);
                        }
                    )
                );

            ASSERT_EQ(newVal, E_FN_SUCCESS);
        }
    );

    std::thread main(
        [&toWorker, &toMain]() {
            putSVar(toWorker, 5);

            auto res = takeSVar(toMain);

            ASSERT_EQ(res, 6);
        }
    );

    worker.join();
    main.join();
}
