#include <FUtils/Ret/Either.h>

#include <include/Stype.h>

#include <gtest/gtest.h>
#include <string>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

enum class RErrCode {
    Success = 0,
    Err = -1
};

TEST(ADTRes, ConstructorByError) {
    Either<double,RErrCode> res(RErrCode::Success);
}

TEST(ADTRes, ConstructorByVal) {
    Either<std::string, RErrCode> res(MLeft<std::string>("T"));
}

TEST(ADTRes, ConstructionWithoutAmbDeduction) {
    Either<int,RErrCode> res(MRight(RErrCode::Success));
}

TEST(ADTRes, AssigmentOperatorADTRes) {
    Either<std::string, RErrCode> res(MLeft<std::string>("T"));
    // Doing some math! Ups, something failed;
    res = RErrCode::Err;

    PMatch(res,
        [](auto string) {
            ASSERT_FALSE(true) << "Either shouldn't be a std::string";
        },
        [](auto errcode) {
            ASSERT_EQ(errcode, RErrCode::Err);
        }
    );
}

TEST(ADTRes, CopyAssignmentOperator) {
    Either<std::string, RErrCode> resS(MLeft<std::string>("T"));
    Either<std::string, RErrCode> resC(MRight(RErrCode::Success));

    resS = resC;

    PMatch(resS,
        [](auto string) {
            ASSERT_FALSE(true) << "Either shouldn't be a std::string";
        },
        [](auto errcode) {
            ASSERT_EQ(errcode, RErrCode::Success);
        }
    );
}

TEST(ADTRes, AssigmentOperatorByVal) {
    Either<std::string, RErrCode> res = MLeft<std::string>("T");

    PMatch(res,
        [](auto string) {
            ASSERT_EQ(string, "T");
        },
        [](auto) {
            ASSERT_FALSE(true) << "Either shouldn't be a error";
        }
    );
}

TEST(ADTRes, AssigmentOperatorByErr) {
    Either<std::string, RErrCode> res = MRight(RErrCode::Success);

    PMatch(res,
        [](auto string) {
            ASSERT_FALSE(true) << "Either shouldn't be a std::string";
        },
        [](auto errcode) {
            ASSERT_EQ(errcode, RErrCode::Success);
        }
    );
}

TEST(ADTRes, MoveConstructor) {
    Either<SType, RErrCode> resR(MLeft(SType {}));
    Either<SType, RErrCode> resM(std::move(resR));

    PMatch(resR,
        [](auto stype) {
            ASSERT_EQ(stype.memory, (uint32_t*)0);
        },
        [](auto) {
            ASSERT_FALSE(true) << "Either shouldn't be error";
        }
    );
    PMatch(resM,
        [](auto stype) {
            ASSERT_NE(stype.memory, (uint32_t*)0);
        },
        [](auto) {
            ASSERT_FALSE(true) << "Either shouldn't be error";
        }
    );
}

TEST(ADTRes, MoveAssignmentOperator) {
    Either<std::string, RErrCode> resR(MRight(RErrCode::Err));
    Either<std::string, RErrCode> resA(MLeft<std::string>("P"));

    resR = std::move(resA);

    PMatch(resR,
        [](auto string) {
            ASSERT_EQ(string, "P");
        },
        [](auto) {
            ASSERT_FALSE(true) << "Either shouldn't be a error";
        }
    );
}

TEST(ADTRes, SafeRMathLambdas) {
    Either<std::string, RErrCode> res(MLeft<std::string>("T"));

    auto mres = PMatch(
        std::move(res)
        , [] (std::string s) {
            return s.length();
        }
        , [] (RErrCode c) {
            return size_t(c);
        }
    );

    ASSERT_EQ(mres, 1);
}

size_t strLength(std::string s) {
    return s.length();
}

size_t returnErrCode(RErrCode c) {
    return size_t(c);
}

TEST(ADTRes, SafeRMathFnPointers) {
    Either<std::string, RErrCode> res(MLeft<std::string>("T"));

    auto mres = PMatch(
        res
        , strLength
        , returnErrCode
    );

    ASSERT_EQ(mres, 1);
}
