#ifndef STYPE_H
#define STYPE_H

#include <cstddef>

class SType {
private:
public:
    uint32_t* memory = NULL;
    ~SType() {
        if (memory != NULL) {
            delete[] memory;
        }
    }

    SType() {
        memory = new uint32_t[100];
    }

    SType(SType&& other) {
        memory = other.memory;
        other.memory = NULL;
    }

    SType(const SType& other) {
        if (other.memory == NULL) {
            memory = NULL;
        } else {
            if (memory == NULL) {
                memory = new uint32_t[100];
            }
            memcpy(memory, other.memory, sizeof(uint32_t)*100);
        }
    }
};

#endif